package com.zhuli.file.model;

/**
 * @author zhuli
 * @description 发送文件流信息
 * @date 2022/9/14
 */
public class FileConfig {

    public FileConfig(String name, int state, long size, int endPos,byte[] bytes) {
        this.name = name;
        this.state = state;
        this.size = size;
        this.endPos = endPos;
        this.bytes = bytes;
    }

    private String name;
    private int state;
    private long size;
    private int endPos;

    private byte[] bytes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getEndPos() {
        return endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

}
