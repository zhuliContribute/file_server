package com.zhuli.file.model;

import lombok.Data;

/**
 * @Description 加载配置文件
 * @Author zhuli
 * @Date 2021/6/7/3:55 PM
 */
@Data
public class VersionConfig {

    private String version;
    private String type;
    private String url;
    private String content;

    public VersionConfig(String version, String type, String url, String content) {
        this.version = version;
        this.type = type;
        this.url = url;
        this.content = content;
    }

}
