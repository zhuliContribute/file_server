package com.zhuli.file.model;

/**
 * @author zhuli
 * @description 文件
 * @date 2022/6/20
 */
public class FileModel {
    private byte[] bytes;
    private String fileName;
    private long size;

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

}
