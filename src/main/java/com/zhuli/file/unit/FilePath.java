package com.zhuli.file.unit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FilePath {

    /**
     * 获取文件所在目录位置
     *
     * @param file
     * @param str
     * @param parent
     */
    public static void getDirectoryAllFilePath(File file, List<Object> str, String parent) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File value : files) {
                if (value.isFile()) {
                    str.add(parent + File.separator + value.getParentFile().getName() + File.separator + value.getName());
                } else {
                    List<Object> s = new ArrayList<>();
                    getDirectoryAllFilePath(value, s, parent + File.separator + value.getParentFile().getName());
                    str.add(s);
                }
            }
        } else {
            str.add(file.getName());
        }
    }
}
