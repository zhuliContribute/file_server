package com.zhuli.file.netty;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhuli
 * @description 消息广播处理中心
 * @date 2022/6/9
 */
public class MsgBroadcastProcessing {

    private static MsgBroadcastProcessing instance;

    public static MsgBroadcastProcessing getInstance() {
        if (instance == null) {
            synchronized (MsgBroadcastProcessing.class) {
                if (instance == null) {
                    instance = new MsgBroadcastProcessing();
                }
            }
        }
        return instance;
    }

    private final ChannelGroup clients = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private List<SendMessageHandler> sendHandlerList = new ArrayList<>();

    public void add(Channel channel) {
        clients.add(channel);
    }

    public void remove(Channel channel) {
        clients.remove(channel);
    }

    /**
     * 除自己外全部发送
     *
     * @param msg
     */
    public void sendNotMe(Channel client, Object msg) {
        for (Channel channel : clients) {
            if (!channel.equals(client)) {
                channel.writeAndFlush(msg);
            }
        }
    }

    /**
     * 封装好的消息发送
     *
     * @param msg
     */
    public void sendAll(Object msg) {
        for (Channel client : clients) {
            client.writeAndFlush(msg);
        }
    }

    /**
     * 指定发送
     *
     * @param host
     * @param msg
     */
    public void sendTo(String host, String msg) {
        for (Channel client : clients) {
            if (client.remoteAddress().equals(host)) {
                client.writeAndFlush(msg);
            }
        }
    }

    /**
     * 注册发送消息器
     */
    public void register(SendMessageHandler sendMessageHandler) {
        sendHandlerList.add(sendMessageHandler);
    }

    /**
     * 注销发送消息器
     */
    public void unregister(SendMessageHandler sendMessageHandler) {
        sendHandlerList.remove(sendMessageHandler);
    }

    /**
     * 封装消息准备发送
     *
     * @param t
     * @param <T>
     */
    public <T> void broadcastAll(T t, Class<T> s) {
        for (SendMessageHandler sendMessageHandler : sendHandlerList) {
            sendMessageHandler.send(t, s);
        }
    }

}
