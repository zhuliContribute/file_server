package com.zhuli.file.netty;

public interface SendMessageHandler {
    <T> void send(T msg, Class<T> s);
}
