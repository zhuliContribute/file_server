package com.zhuli.file.netty.handler;

import com.google.gson.Gson;
import com.zhuli.file.netty.MsgBroadcastProcessing;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * @author zhuli
 * @description 文本 Web 传输
 * @date 2022/9/14
 */
public class WebSocketHandler extends BaseHandler<TextWebSocketFrame> {

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        MsgBroadcastProcessing.getInstance().sendNotMe(ctx.channel(), msg.text());
        System.out.println("["+ getClass().getSimpleName() + "接收]" + new Gson().toJson(msg));
    }

    @Override
    public <T> void send(T msg, Class<T> s) {
        if (s.getSimpleName().equals(TextWebSocketFrame.class.getSimpleName())) {
            System.out.println("["+ getClass().getSimpleName() + "发送]" + new Gson().toJson(msg));
        }
    }
}
