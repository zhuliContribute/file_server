package com.zhuli.file.netty.handler;

import com.google.gson.Gson;
import com.zhuli.file.netty.MsgBroadcastProcessing;
import com.zhuli.file.result.RequestResult;
import io.netty.channel.ChannelHandlerContext;

import java.nio.charset.StandardCharsets;


/**
 * @author zhuli
 * @description RequestResult请求
 * @date 2022/9/14
 */
public class RequestResultHandler extends BaseHandler<RequestResult<String>> {

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, RequestResult<String> msg) throws Exception {
        System.out.println("["+ getClass().getSimpleName() + "]接收" + new Gson().toJson(msg));
        if (msg != null) {
            System.out.println("消息内容：" + msg.getData());
            if (msg.getCode() == 400) {
                MsgBroadcastProcessing.getInstance().remove(ctx.channel());
            } else {
                MsgBroadcastProcessing.getInstance().sendNotMe(ctx.channel(), msg);
            }
        }
    }

    @Override
    public <T> void send(T msg, Class<T> s) {
        if (s.getSimpleName().equals(RequestResult.class.getSimpleName())) {
            System.out.println("["+ getClass().getSimpleName() + "]发送" + new Gson().toJson(msg));
            MsgBroadcastProcessing.getInstance().sendAll(new Gson().toJson(msg).getBytes(StandardCharsets.UTF_8));
        }
    }
}
