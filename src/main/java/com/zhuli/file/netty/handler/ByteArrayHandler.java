package com.zhuli.file.netty.handler;

import com.zhuli.file.netty.MsgBroadcastProcessing;
import io.netty.channel.ChannelHandlerContext;

import java.nio.charset.StandardCharsets;

/**
 * @author zhuli
 * @description 字节传输
 * @date 2022/9/14
 */
public class ByteArrayHandler extends BaseHandler<byte[]> {

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, byte[] msg) throws Exception {
        if (msg != null) {
            String json = new String(msg, StandardCharsets.UTF_8);
            System.out.println("["+ getClass().getSimpleName() + "]" + json);
            ctx.channel().writeAndFlush(("服务器收到内容：" + json).getBytes(StandardCharsets.UTF_8));
        }
    }


    @Override
    public <T> void send(T msg, Class<T> s) {
        if (s.getSimpleName().equals(byte[].class.getSimpleName())) {
            String path = new String((byte[]) msg, StandardCharsets.UTF_8);
            System.out.println("["+ getClass().getSimpleName() + "发送]" + path);
            MsgBroadcastProcessing.getInstance().sendAll(msg);
        }
    }

}
