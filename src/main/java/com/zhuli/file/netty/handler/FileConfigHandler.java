package com.zhuli.file.netty.handler;

import com.google.gson.Gson;
import com.zhuli.file.model.FileConfig;
import com.zhuli.file.netty.MsgBroadcastProcessing;
import com.zhuli.file.netty.SocketInitializer;
import com.zhuli.file.request.FileController;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


/**
 * @author zhuli
 * @description 文件传输
 * @date 2022/9/14
 */
public class FileConfigHandler extends BaseHandler<FileConfig> {

    private volatile FileInputStream inputStream;
    private volatile File file;

    @Override
    protected synchronized void messageReceived(ChannelHandlerContext ctx, FileConfig config) throws Exception {

        try {
            if (config.getEndPos() > 0 && config.getSize() > 0) {
                System.out.print("[进度：" + getClass().getSimpleName() + "]：" + (((config.getEndPos() + 0.0f) / config.getSize()) * 100) + "\r");
            }
            if (config.getState() == 1) {//传输文件
                //本次发送数量
                int size = 0;
                if (config.getSize() - config.getEndPos() >= SocketInitializer.FrameSize) {
                    size = SocketInitializer.FrameSize;
                } else {
                    size = (int) config.getSize() - config.getEndPos();
                }

                if (size <= 0) {
                    finish(ctx.channel(), config, -1);
                } else {
                    //剩余大小和有效大小不一致时说明客户端已有部分数据，断点续传
                    if ((config.getSize() - config.getEndPos()) != inputStream.available()) {
                        //需要跳过的字节
                        int abandon = inputStream.available() - ((int) config.getSize() - config.getEndPos());
                        //跳过数量
                        inputStream.skip(abandon);
                        System.out.println("[" + getClass().getSimpleName() + "]跳过数量：" + abandon);
                    }
                    byte[] bytes = new byte[size];
                    if (inputStream.read(bytes) != -1) {
                        ctx.channel().writeAndFlush(new Gson().toJson(new FileConfig(config.getName(), 1, config.getSize(), config.getEndPos() + size, bytes)).getBytes(StandardCharsets.UTF_8));
                    }
                }

            } else if (config.getState() == 2) {//请求文件
                if (file == null) {
                    file = new File(FileController.filePath + config.getName());
                }
                if (inputStream == null) {
                    if (file.exists() && file.isFile()) {
                        inputStream = new FileInputStream(file);
                        ctx.channel().writeAndFlush(new Gson().toJson(new FileConfig(config.getName(), 2, file.length(), 0, new byte[0])).getBytes(StandardCharsets.UTF_8));
                        System.out.println("创建文件流 hashCode：" + hashCode());
                    }
                }
            } else if (config.getState() == -1) {//完成
                finish(ctx.channel(), config, -1);

            } else if (config.getState() == -2) {//错误
                finish(ctx.channel(), config, -2);
            }

        } catch (Exception e) {
            finish(ctx.channel(), config, -2);
        }

    }


    /**
     * 完成
     *
     * @param channel
     */
    private void finish(Channel channel, FileConfig config, int state) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        file = null;
        channel.writeAndFlush(new FileConfig(config.getName(), state, config.getSize(), config.getEndPos(), new byte[0]));
        System.out.println("[" + getClass().getSimpleName() + "]传输完成 hashCode：" + hashCode());
    }

    @Override
    public <T> void send(T msg, Class<T> s) {
        if (s.getSimpleName().equals(FileConfig.class.getSimpleName())) {
            System.out.println("[" + getClass().getSimpleName() + "推送文件]" + new Gson().toJson(msg));
            FileConfig config = (FileConfig) msg;
            file = new File(FileController.filePath + config.getName());
            if (file.exists() && file.isFile()) {
                MsgBroadcastProcessing.getInstance().sendAll(new Gson().toJson(msg).getBytes(StandardCharsets.UTF_8));
            }
        }

    }
}
