package com.zhuli.file.netty.handler;

import com.zhuli.file.netty.MsgBroadcastProcessing;
import com.zhuli.file.netty.SendMessageHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public abstract class BaseHandler<T> extends SimpleChannelInboundHandler<T> implements SendMessageHandler {

    /**
     * 客户端加入
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println("["+ getClass().getSimpleName() + "加入]" + ctx.channel().remoteAddress());
        MsgBroadcastProcessing.getInstance().add(ctx.channel());
        MsgBroadcastProcessing.getInstance().register(this);
    }


    /**
     * 客户端离开
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        System.out.println("["+ getClass().getSimpleName() + "移除]" + ctx.channel().remoteAddress());
        MsgBroadcastProcessing.getInstance().remove(ctx.channel());
        MsgBroadcastProcessing.getInstance().unregister(this);
    }


    /**
     * 活跃状态
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("["+ getClass().getSimpleName() + "上线]" + ctx.channel().remoteAddress());
    }


    /**
     * 不活跃状态
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("["+ getClass().getSimpleName() + "下线]" + ctx.channel().remoteAddress());
        MsgBroadcastProcessing.getInstance().remove(ctx.channel());
        MsgBroadcastProcessing.getInstance().unregister(this);
    }


    /**
     * 客户端退出
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        MsgBroadcastProcessing.getInstance().remove(ctx.channel());
        MsgBroadcastProcessing.getInstance().unregister(this);
        System.out.println("["+ getClass().getSimpleName() + "异常捕获]" + ctx.channel() + ":" + "退出");
    }


}
