package com.zhuli.file.netty;

import com.zhuli.file.netty.handler.ByteArrayHandler;
import com.zhuli.file.netty.coder.*;
import com.zhuli.file.netty.handler.FileConfigHandler;
import com.zhuli.file.netty.handler.RequestResultHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author zhuli
 * @description 通道初始化器
 * @date 2022/6/9
 */
public class SocketInitializer extends ChannelInitializer<SocketChannel> {

    public static final int FrameSize = 1024 * 256;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        //通道管道
        ChannelPipeline pipeline = ch.pipeline();

//        pipeline.addLast(
//                //基于分隔符的帧解码器
//                new DelimiterBasedFrameDecoder(1024, Delimiters.lineDelimiter()),
//                //心跳机制
//                new IdleStateHandler(60, 60, 60, TimeUnit.SECONDS),
//                //字符串解码器
//                new StringDecoder(CharsetUtil.UTF_8),
//                //字符串编码器
//                new StringEncoder(CharsetUtil.UTF_8),
//                //客户端socket消息处理器
////                new ClientSocketHandler(),
//                //file
//                new ClientFileHandler()
//
////                //Http 服务器编解码器
////                new HttpServerCodec(),
////                //分块编写处理程序
////                new ChunkedWriteHandler(),
////                //将分段请求集合到一起
////                new HttpObjectAggregator(1024),
////                //Web Socket服务器协议处理程序
////                new WebSocketServerProtocolHandler("/ws"),
////                //浏览器socket消息处理器
////                new WebSocketHandler()
//        );

//        pipeline.addLast(new DelimiterBasedFrameDecoder(1024, Delimiters.lineDelimiter()));
        pipeline.addLast(new LengthFieldBasedFrameDecoder(FrameSize * 4, 0, 4, 0, 4));
        pipeline.addLast(new IdleStateHandler(60, 60, 60, TimeUnit.SECONDS));

        pipeline.addLast(new LengthFieldPrepender(4));
        //使用string解码器的话byte[]会默认转成string
//        pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));
//        pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));

        pipeline.addLast(new ByteArrayDecoder());
        pipeline.addLast(new ByteArrayEncoder());

        pipeline.addLast(new RequestResultEncoder());
        pipeline.addLast(new RequestResultDecoder());

        pipeline.addLast(new FileConfigEncoder());
        pipeline.addLast(new FileConfigDecoder());

        pipeline.addLast(new FileConfigHandler());
        pipeline.addLast(new ByteArrayHandler());
        pipeline.addLast(new RequestResultHandler());

    }
}
