package com.zhuli.file.netty.coder;

import com.google.gson.Gson;
import com.zhuli.file.model.FileConfig;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.util.CharsetUtil;

import java.nio.CharBuffer;
import java.util.List;

public class FileConfigEncoder extends MessageToMessageEncoder<FileConfig> {

    @Override
    protected void encode(ChannelHandlerContext ctx, FileConfig s, List<Object> list) throws Exception {
        list.add((ByteBufUtil.encodeString(ctx.alloc(), CharBuffer.wrap(new Gson().toJson(s)), CharsetUtil.UTF_8)));
//        System.out.println("["+ getClass().getSimpleName() + "]" + new Gson().toJson(s));
    }

}
