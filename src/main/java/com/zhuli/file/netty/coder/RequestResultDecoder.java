package com.zhuli.file.netty.coder;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.zhuli.file.result.RequestResult;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class RequestResultDecoder extends MessageToMessageDecoder<byte[]> {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, byte[] s, List<Object> list) throws Exception {
//        System.out.println("["+ getClass().getSimpleName() + "]" + new String(s, StandardCharsets.UTF_8));
        list.add(new Gson().fromJson(new String(s, StandardCharsets.UTF_8), RequestResult.class));
    }

    @Override
    public boolean acceptInboundMessage(Object msg) throws Exception {
        if (msg instanceof byte[]) {
            try {
                RequestResult result = new Gson().fromJson(new String((byte[]) msg,StandardCharsets.UTF_8), RequestResult.class);
                if (result != null && result.getCode() != null && result.getMessage() != null && result.getData() != null) {
                    return true;
                }
            } catch (JsonSyntaxException e) {
                return false;
            }
        }
        return false;
    }
}
