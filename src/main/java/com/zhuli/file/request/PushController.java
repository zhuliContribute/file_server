package com.zhuli.file.request;

import com.zhuli.file.model.FileConfig;
import com.zhuli.file.model.FileModel;
import com.zhuli.file.model.VersionConfig;
import com.zhuli.file.netty.MsgBroadcastProcessing;
import com.zhuli.file.result.RequestResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

@RequestMapping("api/push")
@Controller
public class PushController {

    @RequestMapping("/msg")
    public String sendMsg() {
        return "msgPush";
    }

    /**
     * 推送消息
     *
     * @return
     */
    @PostMapping(value = "/msg")
    public @ResponseBody
    RequestResult pushMsg(@RequestParam(value = "type") String type, @RequestParam(value = "value") Object value) {
        System.out.println("推送消息:" + type + " value" + value);
        if (type.equals("msg")) {
            MsgBroadcastProcessing.getInstance().broadcastAll(RequestResult.success(301,"推送消息",value),RequestResult.class);
            return RequestResult.success("msg send success");
        }
        return RequestResult.success("type neglect！");
    }

    /**
     * 推送更新
     *
     * @return
     */
    @PostMapping(value = "/update")
    public @ResponseBody
    RequestResult pushUpdate(@RequestParam(value = "version") String version,
                             @RequestParam(value = "type") String type,
                             @RequestParam(value = "url") String url,
                             @RequestParam(value = "content") String content) {
        System.out.println("推送更新:" + content);
        MsgBroadcastProcessing.getInstance().broadcastAll(RequestResult.success(302,"推送更新",new VersionConfig(version, type, url, content)), RequestResult.class);
        return RequestResult.success(new VersionConfig(version, type, url, content));
    }

    /**
     * 推送文件
     *
     * @return
     */
    @PostMapping(value = "/pushFile")
    public @ResponseBody
    RequestResult pushFile(@RequestParam(value = "type") String type,
                           @RequestParam(value = "fileName") String fileName) throws IOException {
        System.out.println("推送文件:" + FileController.filePath + fileName);
        File file = new File(FileController.filePath + fileName);
        if (file.exists() && file.isFile()) {

            if (type.equals("1")) {
                //发送文件
                FileModel fileModel = new FileModel();
                FileInputStream inputStream = new FileInputStream(file);
                byte[] copyBytes = new byte[0];
                byte[] bytes = new byte[1024];
                int count = 0;
                while ((count = inputStream.read(bytes)) != -1) {
                    copyBytes = Arrays.copyOf(copyBytes, copyBytes.length + count);
                    System.arraycopy(bytes, 0, copyBytes, copyBytes.length - count, count);
                }
                inputStream.close();
                fileModel.setBytes(bytes);
                fileModel.setFileName(file.getName());
                fileModel.setSize(file.length());

                MsgBroadcastProcessing.getInstance().sendAll(fileModel);

            } else {

                //广播消息
//                MsgBroadcastProcessing.getInstance().broadcastAll(file.getAbsolutePath());

//                MsgBroadcastProcessing.getInstance().sendAll(RequestResult.success(file.getAbsolutePath()));

                MsgBroadcastProcessing.getInstance().broadcastAll(new FileConfig(file.getName(), 0, file.length(), 0, new byte[0]), FileConfig.class);

            }

            return RequestResult.success(fileName);
        }
        return RequestResult.error(404, "文件不存在:" + fileName);
    }

}
