package com.zhuli.file.request;

import com.zhuli.file.model.VersionModel;
import com.zhuli.file.result.RequestResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author zhuli
 * @description 版本检测
 * @date 2021/7/1 0:05
 */
@RequestMapping("api/app/")
@Controller
public class VersionController {

    /**
     * 版本检测
     *
     * @return
     */
    @GetMapping(value = "version")
    public @ResponseBody
    RequestResult version() {
        return RequestResult.success(VersionModel.getVersionConfig());
    }

}
