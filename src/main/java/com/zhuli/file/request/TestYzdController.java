package com.zhuli.file.request;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhuli
 * @description 文件上传, 下载，获取列表
 * @date 2021/6/30 23:52
 */
@RequestMapping("api/zhkt")
@Controller
public class TestYzdController {

    @RequestMapping(value = "/getDesktops")
    public @ResponseBody String getDesktops(@RequestParam(value = "sn") String sn) {
        System.out.println("sn : " + sn);
        return "{\n" +
                "  \"code\": 200,\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"desktopId\": 1578811,\n" +
                "      \"desktopState\": 1,\n" +
                "      \"cpu\": 2,\n" +
                "      \"memory\": 2,\n" +
                "      \"storage\": 40,\n" +
                "      \"network\": 2,\n" +
                "      \"gpu\": \"--\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"msg\": \"ok\",\n" +
                "  \"time\": \"1693230976\"\n" +
                "}";
    }

    @RequestMapping(value = "/callback")
    public @ResponseBody String callback(@RequestBody String json) {
        System.out.println("json : " + json);
        return new Gson().toJson(new Result(1, new Error(0, "ok")));
    }

    class Result {
        public int success;
        public Error error;

        public Result(int success, Error error) {
            this.success = success;
            this.error = error;
        }
    }

    class Error {
        public int code;
        public String message;

        public Error(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

}