package com.zhuli.file.request;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author zhuli
 * @description 文件上传, 下载，获取列表
 * @date 2021/6/30 23:52
 */
@RequestMapping("api/file")
@Controller
public class WebFileController {

    @RequestMapping("/fileUpload")
    public String getFilePath() {
        return "uploadFile";
    }

    @RequestMapping("/fileDownload")
    public String FileDownload() {
        return "downloadFile";
    }

}
