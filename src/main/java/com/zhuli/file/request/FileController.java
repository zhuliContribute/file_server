package com.zhuli.file.request;

import com.zhuli.file.result.RequestResult;
import com.zhuli.file.result.ResultCode;
import com.zhuli.file.unit.FilePath;
import com.zhuli.file.unit.FileType;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author zhuli
 * @description 文件上传, 下载，获取列表
 * @date 2021/6/30 23:52
 */
@RequestMapping("api/file")
@Controller
public class FileController {

    //文件路径
//    @Value("${filepath}")
    public static String filePath;

    public FileController() {
        filePath = getPath() + File.separator + "file" + File.separator;
        File file = new File(filePath);
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
        }
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/get", produces = "application/json;charset=UTF-8")
    public void getFileOnlineCat(HttpServletRequest request, @RequestParam(value = "filename") String fileName, HttpServletResponse response) { //@PathVariable("filename") String fileName

        InputStream is = null;
        OutputStream os = null;
        try {
            File file = new File(filePath + fileName);
            if (file.isFile() && file.exists()) {

                if (FileType.checkRecords(fileName)) {

                    response.setContentType(FileType.getContentType(fileName));
                    response.addHeader("Content-Length", "" + file.length());
                    is = Files.newInputStream(file.toPath());
                    os = response.getOutputStream();
                    IOUtils.copy(is, os);

                } else {

                    response.setContentType("application/json;charset=UTF-8");
                    is = Files.newInputStream(file.toPath());
                    byte[] b = new byte[1024];
                    StringBuffer str = new StringBuffer();
                    while (is.read(b) != -1) {
                        str.append(new String(b, StandardCharsets.UTF_8));
                    }
                    response.getWriter().write(str.toString());
                }

            } else {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write("The file does not exist !");
            }
        } catch (Exception e) {
            System.err.println("play video error : " + e.getMessage());
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {

                }
            }
            if (null != os) {
                try {
                    os.close();
                } catch (IOException e) {

                }
            }
        }
    }


    /**
     * 文件上传
     *
     * @param file 文件
     * @return
     */
    @PostMapping(value = "/upload")
    public @ResponseBody
    RequestResult upload(
            @RequestParam(value = "file") MultipartFile file, HttpServletRequest request) {

        if (file.isEmpty()) {
            System.out.println("文件为空");
            return RequestResult.error(ResultCode.PARAM_IS_INVALID);
        }
        // 获取原始文件名
        String fileName = file.getOriginalFilename();
        // 后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        System.out.println("receive web upload file：" + fileName);
        fileName = UUID.randomUUID() + suffixName;

        // 创建新文件
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }

        try {
            file.transferTo(dest);
            String urlPath = request.getRequestURL().toString().replace("api/file/upload", "files/");
            return RequestResult.success(urlPath + fileName);

        } catch (IOException e) {
            System.out.println("文件上传失败" + e.getMessage());
            return RequestResult.error(402, e.getMessage());

        }

    }


    /**
     * 文件下载
     *
     * @param filename 文件名
     * @return 正确返回类型FileSystemResource
     */
    @PostMapping(value = "/download")
    public @ResponseBody
    ResponseEntity download(
            @RequestParam(value = "filename") String filename) {

        if (filename == null || filename.isEmpty()) {
            System.out.println("文件为空");
            return null;
        }

        File file = Paths.get(filePath).resolve(filename).toFile();
        System.out.println("文件请求路径" + file.getPath());

        if (file.exists() && file.canRead()) {
            System.out.println("文件请求成功");
            return ResponseEntity.ok()
                    .contentType(FileType.getFileType(filename))
                    .body(new FileSystemResource(file));
        }
        System.out.println("文件不存在");
        return ResponseEntity.badRequest().body(RequestResult.error(ResultCode.PARAM_IS_INVALID));
    }


    /**
     * 获取文件列表
     */
    @GetMapping(value = "/getFileList")
    public @ResponseBody
    ResponseEntity getFileList() {
        System.out.println("加载文件");
        File file = new File(filePath);
        if (file.exists() && file.isDirectory()) {
            List<Object> fileNames = new ArrayList<>();
            FilePath.getDirectoryAllFilePath(file, fileNames, "");
            return ResponseEntity.ok().body(RequestResult.success(fileNames));
        }
        return ResponseEntity.badRequest().body(RequestResult.error(ResultCode.PARAM_IS_INVALID));
    }


    /**
     * 获取apk下载地址
     *
     * @param packageName 文件名
     * @return
     */
    @PostMapping(value = "/getApk")
    public @ResponseBody
    RequestResult getFile(
            @RequestParam(value = "packageName") String packageName) {

        if (packageName.isEmpty()) {
            System.out.println("请求 apk 包名为空");
            return RequestResult.error(ResultCode.PARAM_IS_INVALID);
        }

        String file = "";

        switch (packageName) {
            case "com.msgbyte.tailchat":
                file = "Tailchat.apk";
                break;
            default:
                break;
        }

        if (file.isEmpty()) {
            System.out.println("apk 未添加");
            return RequestResult.error(404, "apk 未找到");
        } else {
            return RequestResult.success("http://192.168.1.138:8088/files/" + file);
        }

    }


    /**
     * 获取本地路径下文件
     *
     * @return
     */
    private String getPath() {
        String path = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        if (System.getProperty("os.name").contains("dows")) {
            path = path.substring(1);
        }
        if (path.contains("jar")) {
            path = path.substring(0, path.lastIndexOf("."));
            return path.substring(0, path.lastIndexOf("/"));
        }
        return path.replace("target/classes/", "");
    }

}
