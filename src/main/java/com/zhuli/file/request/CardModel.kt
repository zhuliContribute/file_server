package com.zhuli.file.request


/**
 * Copyright (C) 王字旁的理
 * Date: 2023/2/26
 * Description: 页面展示数据
 * Author: zl
 */

data class User(
    val id: Int = 0,
    var phone: String,
    var password: String,
    var sn: String = "",//SN码
    var profile: String? = "",//头像
    var vip: Long = 0L,//有效期时间戳
)

/**
 * 卡片行数据
 */
data class RowCard(
    val title: String? = null,
    var type: Int = 0,
    var cardArray: Array<CardData>? = null
)

/**
 * 卡片基础信息
 */
data class CardData(
    val id: Int = 0,
    val imgUrl: String? = null,
    val vip: Int? = 0
)

data class VideoModel(
    val id: Int = 0,//视频id
    val title: String? = null,//课程标题
    val introduce: String? = null,//介绍
    val source: String? = null,//出处
    val see: Int = 0,//查看次数
    val itemCount: Int = 0,//章节数量
    val bgUrl: String? = null,//视频背景
    val recommendArray: Array<CardData>? = null,//推荐课程
)

data class VideoItemInfo(var title: String = "", var url: String = "")