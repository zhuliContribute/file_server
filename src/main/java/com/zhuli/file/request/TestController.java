package com.zhuli.file.request;

import com.zhuli.file.result.RequestResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhuli
 * @description 文件上传, 下载，获取列表
 * @date 2021/6/30 23:52
 */
@RequestMapping("api/zhkt")
@Controller
public class TestController {

    private final static String baseUrl = "https://6545acb3.r7.cpolar.top";
//    private final static String baseUrl = "http://192.168.1.60:8088";

    @PostMapping(value = "/user/userinfo")
    public @ResponseBody RequestResult queryUser(@RequestParam(value = "sn") String sn) {
        System.out.println("sn : " + sn);
        return RequestResult.success(new User(1, "10086", "1234", "AAAAAAA", baseUrl + "/api/file/get?filename=aaa.png", new Date().getTime() + 10000));
    }

    @PostMapping(value = "/index/title")
    public @ResponseBody RequestResult getTitles(@RequestParam(value = "sn") String sn) {
        System.out.println("sn : " + sn);
        String[] titleArray = new String[]{"幼教", "小学", "初中", "高中",};
        return RequestResult.success(titleArray);
    }

    @PostMapping(value = "/index/info")
    public @ResponseBody RequestResult getPage(@RequestParam(value = "sn") String sn, @RequestParam(value = "page") int page) {
        System.out.println("sn : " + sn + " page : " + page);

        List<RowCard> pageData = new ArrayList<>();
        pageData.add(new RowCard("第" + page + "年级", 0, new CardData[]{
                new CardData(1, baseUrl + "/api/file/get?filename=img_card_speaker.png", 0),
                new CardData(2, baseUrl + "/api/file/get?filename=img_card_storage.png", 1),
                new CardData(3, baseUrl + "/api/file/get?filename=img_card_texture.png", 0),
                new CardData(4, baseUrl + "/api/file/get?filename=img_card_thin.png", 0)}
        ));

        pageData.add(new RowCard("第" + page + "年级", 0, new CardData[]{
                new CardData(5, baseUrl + "/api/file/get?filename=img_card_speaker.png", 0),
                new CardData(6, baseUrl + "/api/file/get?filename=img_card_storage.png", 1),
                new CardData(7, baseUrl + "/api/file/get?filename=img_card_texture.png", 0),
                new CardData(8, baseUrl + "/api/file/get?filename=img_card_thin.png", 0)}
        ));

        pageData.add(new RowCard("第" + page + "年级", 0, new CardData[]{
                new CardData(9, baseUrl + "/api/file/get?filename=img_card_speaker.png", 0),
                new CardData(10, baseUrl + "/api/file/get?filename=img_card_storage.png", 1),
                new CardData(11, baseUrl + "/api/file/get?filename=img_card_texture.png", 0),
                new CardData(12, baseUrl + "/api/file/get?filename=img_card_thin.png", 0)}
        ));

        pageData.add(new RowCard("第" + page + "年级", 0, new CardData[]{
                new CardData(13, baseUrl + "/files/img_card_speaker.png", 0),
                new CardData(14, baseUrl + "/files/img_card_storage.png", 1),
                new CardData(15, baseUrl + "/files/img_card_texture.png", 0),
                new CardData(16, baseUrl + "/files/img_card_thin.png", 0)}
        ));

        return RequestResult.success(pageData);
    }

    @PostMapping(value = "/video/info")
    public @ResponseBody RequestResult getVideoDetails(@RequestParam(value = "sn") String sn, @RequestParam(value = "id") int id) {
        System.out.println("sn : " + sn + " id " + id);
        VideoModel data = new VideoModel(id, "语文", "教育出版社", "国文", 100, 36, "https://img0.baidu.com/it/u=3493541977,1767594769&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500",
                new CardData[]{
//                        new CardData(13, baseUrl + "/api/file/get?filename=img_card_speaker.png", 1),
//                        new CardData(14, baseUrl + "/api/file/get?filename=img_card_storage.png", 0)
                        new CardData(14, baseUrl + "/files/img_card_storage.png", 1),
                        new CardData(14, baseUrl + "/files/img_card_storage.png", 0)
                });
        return RequestResult.success(data);
    }

    @PostMapping(value = "/video/chapter")
    public @ResponseBody RequestResult getVideoInfoList(@RequestParam(value = "sn") String sn, @RequestParam(value = "id") int id, @RequestParam(value = "start") int start, @RequestParam(value = "end") int end) {
        System.out.println("sn : " + sn + " id " + id + " start " + start + " end " + end);
        List<VideoItemInfo> data = new ArrayList<>();
        for (int i = start; i < end + 1; i++) {
//            data.add(new VideoItemInfo("第" + (i + 1) + "集", baseUrl + "/api/file/get?filename=test1.mp4"));
            data.add(new VideoItemInfo("第" + (i + 1) + "集", baseUrl + "/files/test1.mp4"));
//            data.add(new VideoItemInfo("第" + (start + i + 1) + "集", baseUrl + "/api/file/get?filename=test1.mp4"));
//            data.add(new VideoItemInfo("第" + (i + 1) + "集", "https://vd2.bdstatic.com/mda-kjnmfakn7t2dn3d6/v1-cae/sc/mda-kjnmfakn7t2dn3d6.mp4"));
        }
        return RequestResult.success(data);
    }

}